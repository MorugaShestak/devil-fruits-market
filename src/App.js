import './App.css';
import DevilFruit from "./components/DevilFruit";
import {useEffect, useState} from "react";

function App() {

    const [fruits, setFruits] = useState()
    const [input, setInput] = useState("")

    // async function getFruits() {
    //     let fruits = document.getElementsByClassName("fruit")
    //     await setFruits(fruits)
    //     console.log(fruits)
    // }

    useEffect(() => {
        let fruits = document.querySelectorAll(".card")
        let details = document.querySelectorAll("details")
        for(let i = 0; i < fruits.length; i++) {
            if(fruits[i].children[0].innerHTML.includes(input) === false) {
                fruits[i].classList.add("hide")
            }
            else {
                fruits[i].classList.remove("hide")
            }
        }
    }, [input])

  return (
    <div className="App" >
        <input placeholder={"Введите сюда название фрукта для сортировки"} className={"nameSearching"} type={"text"} onChange={(e) => {
            setInput(e.target.value)}}/>

      <details>
        <summary className={"zoan"}>
            <b>Зоан</b>
        </summary>

          <DevilFruit name={"Уси Уси но Ми, модель: Бизон"} type={"zoan"} rank={"C"}
                      url={"https://onepiece.fandom.com/ru/wiki/%D0%A3%D1%81%D0%B8_%D0%A3%D1%81%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%91%D0%B8%D0%B7%D0%BE%D0%BD"}/>
          <DevilFruit name={"Уси Уси но Ми, модель: Жираф"} type={"zoan"} rank={"C"}
                      url={"https://onepiece.fandom.com/ru/wiki/%D0%A3%D1%81%D0%B8_%D0%A3%D1%81%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%96%D0%B8%D1%80%D0%B0%D1%84"} />
          <DevilFruit name={"Тори Тори но Ми, модель: Сокол"} type={"zoan"} rank={"C"}
                      url={"https://onepiece.fandom.com/ru/wiki/%D0%A2%D0%BE%D1%80%D0%B8_%D0%A2%D0%BE%D1%80%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9E%D1%80%D1%91%D0%BB"} />
          <DevilFruit name={"Ину Ину но Ми, модель: Волк"} type={"zoan"} rank={"C"}
                      url={"https://onepiece.fandom.com/ru/wiki/%D0%98%D0%BD%D1%83_%D0%98%D0%BD%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%92%D0%BE%D0%BB%D0%BA"} />
          <DevilFruit name={"Нэко Нэко но Ми, модель: Леопард"} type={"zoan"} rank={"C"}
                      url={"https://onepiece.fandom.com/ru/wiki/%D0%9D%D1%8D%D0%BA%D0%BE_%D0%9D%D1%8D%D0%BA%D0%BE_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9B%D0%B5%D0%BE%D0%BF%D0%B0%D1%80%D0%B4"} />
          <DevilFruit name={"Сара Сара но Ми, модель: Аксолотль"} type={"zoan"} rank={"D"}
                      url={"https://onepiece.fandom.com/ru/wiki/%D0%A1%D0%B0%D1%80%D0%B0_%D0%A1%D0%B0%D1%80%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%90%D0%BA%D1%81%D0%BE%D0%BB%D0%BE%D1%82%D0%BB%D1%8C"} />
      </details>
        <details>
            <summary className={"oldZoan"}>
                <b>Древний Зоан</b>
            </summary>

            <DevilFruit name={"Дзо Дзо но Ми, модель: Мамонт"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%94%D0%B7%D0%BE_%D0%94%D0%B7%D0%BE_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9C%D0%B0%D0%BC%D0%BE%D0%BD%D1%82"} />
            <DevilFruit name={"Рю Рю но Ми, модель: Аллозавр"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A0%D1%8E_%D0%A0%D1%8E_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%90%D0%BB%D0%BB%D0%BE%D0%B7%D0%B0%D0%B2%D1%80"} />
            <DevilFruit name={"Рю Рю но Ми, модель: Спинозавр"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A0%D1%8E_%D0%A0%D1%8E_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%A1%D0%BF%D0%B8%D0%BD%D0%BE%D0%B7%D0%B0%D0%B2%D1%80"} />
            <DevilFruit name={"Рю Рю но Ми, модель: Птеранодон"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A0%D1%8E_%D0%A0%D1%8E_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9F%D1%82%D0%B5%D1%80%D0%B0%D0%BD%D0%BE%D0%B4%D0%BE%D0%BD"} />
            <DevilFruit name={"Рю Рю но Ми, модель: Брахиозавр"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A0%D1%8E_%D0%A0%D1%8E_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%91%D1%80%D0%B0%D1%85%D0%B8%D0%BE%D0%B7%D0%B0%D0%B2%D1%80"} />
            <DevilFruit name={"Рю Рю но Ми, модель: Пахицефалозавр"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A0%D1%8E_%D0%A0%D1%8E_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9F%D0%B0%D1%85%D0%B8%D1%86%D0%B5%D1%84%D0%B0%D0%BB%D0%BE%D0%B7%D0%B0%D0%B2%D1%80"} />
            <DevilFruit name={"Рю Рю но Ми, модель: Трицератопс"} type={"oldZoan"} rank={"B"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A0%D1%8E_%D0%A0%D1%8E_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%A2%D1%80%D0%B8%D1%86%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D0%BF%D1%81"} />
        </details>

            <details>
                <summary className={"paramecia"}>
                    <b>Парамецея</b>
                </summary>
                <DevilFruit name={"Бара Бара но Ми"} type={"paramecia"} rank={"C"} url={"https://onepiece.fandom.com/ru/wiki/%D0%91%D0%B0%D1%80%D0%B0_%D0%91%D0%B0%D1%80%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Хана Хана но Ми"} type={"paramecia"} rank={"C"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A5%D0%B0%D0%BD%D0%B0_%D0%A5%D0%B0%D0%BD%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Ори Ори но Ми"} type={"paramecia"} rank={"C"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9E%D1%80%D0%B8_%D0%9E%D1%80%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Саби Саби но Ми"} type={"paramecia"} rank={"D"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A1%D0%B0%D0%B1%D0%B8_%D0%A1%D0%B0%D0%B1%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Ёми Ёми но Ми"} type={"paramecia"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%81%D0%BC%D0%B8_%D0%81%D0%BC%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Кагэ Кагэ но Ми"} type={"paramecia"} rank={"B"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9A%D0%B0%D0%B3%D1%8D_%D0%9A%D0%B0%D0%B3%D1%8D_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Опэ Опэ но Ми"} type={"paramecia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9E%D0%BF%D1%8D_%D0%9E%D0%BF%D1%8D_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Гура Гура но Ми"} type={"paramecia"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%93%D1%83%D1%80%D0%B0_%D0%93%D1%83%D1%80%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Кира Кира но Ми"} type={"paramecia"} rank={"B"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9A%D0%B8%D1%80%D0%B0_%D0%9A%D0%B8%D1%80%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Фува Фува но Ми"} type={"paramecia"} rank={"B"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A4%D1%83%D0%B2%D0%B0_%D0%A4%D1%83%D0%B2%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Бари Бари но Ми"} type={"paramecia"} rank={"B"} url={"https://onepiece.fandom.com/ru/wiki/%D0%91%D0%B0%D1%80%D0%B8_%D0%91%D0%B0%D1%80%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Наги Наги но Ми"} type={"paramecia"} rank={"D"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9D%D0%B0%D0%B3%D0%B8_%D0%9D%D0%B0%D0%B3%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Сору Сору но Ми"} used={"used"} type={"paramecia"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A1%D0%BE%D1%80%D1%83_%D0%A1%D0%BE%D1%80%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Токи Токи но Ми"} type={"paramecia"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A2%D0%BE%D0%BA%D0%B8_%D0%A2%D0%BE%D0%BA%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Дзуси Дзуси но Ми"} type={"paramecia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%94%D0%B7%D1%83%D1%81%D0%B8_%D0%94%D0%B7%D1%83%D1%81%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Дзики Дзики но Ми"} type={"paramecia"} rank={"B"} url={"https://onepiece.fandom.com/ru/wiki/%D0%94%D0%B7%D0%B8%D0%BA%D0%B8_%D0%94%D0%B7%D0%B8%D0%BA%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
            </details>

            <details>
                <summary className={"logia"}>
                    <b>Логия</b>
                </summary>



                <DevilFruit name={"Моку Моку но Ми"} type={"logia"} rank={"A"}
                            url={"https://onepiece.fandom.com/ru/wiki/%D0%9C%D0%BE%D0%BA%D1%83_%D0%9C%D0%BE%D0%BA%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Мэра Мэра но Ми"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9C%D1%8D%D1%80%D0%B0_%D0%9C%D1%8D%D1%80%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Суна Суна но Ми"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A1%D1%83%D0%BD%D0%B0_%D0%A1%D1%83%D0%BD%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Горо Горо но Ми"} type={"logia"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%93%D0%BE%D1%80%D0%BE_%D0%93%D0%BE%D1%80%D0%BE_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Хиэ Хиэ но Ми"} type={"logia"} used={"used"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%A5%D0%B8%D1%8D_%D0%A5%D0%B8%D1%8D_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Пика Пика но Ми"} type={"logia"} used={"used"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9F%D0%B8%D0%BA%D0%B0_%D0%9F%D0%B8%D0%BA%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Ями Ями но Ми"} type={"logia"} used={"used"} rank={"S"} url={"https://onepiece.fandom.com/ru/wiki/%D0%AF%D0%BC%D0%B8_%D0%AF%D0%BC%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Магу Магу но Ми"} used={"used"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9C%D0%B0%D0%B3%D1%83_%D0%9C%D0%B0%D0%B3%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Нума Нума но Ми"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9D%D1%83%D0%BC%D0%B0_%D0%9D%D1%83%D0%BC%D0%B0_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Гасу Гасу но Ми"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%93%D0%B0%D1%81%D1%83_%D0%93%D0%B0%D1%81%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Юки Юки но Ми"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%AE%D0%BA%D0%B8_%D0%AE%D0%BA%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
                <DevilFruit name={"Мори Мори но Ми"} type={"logia"} rank={"A"} url={"https://onepiece.fandom.com/ru/wiki/%D0%9C%D0%BE%D1%80%D0%B8_%D0%9C%D0%BE%D1%80%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8"} />

            </details>

        <details>
            <summary className={"mythZoan"}>
                <b>Мифический Зоан</b>
            </summary>

            <DevilFruit name={"Хито Хито но Ми, модель: Дайбуцу"} used={"used"} type={"mythZoan"} rank={"S"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A5%D0%B8%D1%82%D0%BE_%D0%A5%D0%B8%D1%82%D0%BE_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%94%D0%B0%D0%B9%D0%B1%D1%83%D1%86%D1%83"} />
            <DevilFruit name={"Хито Хито но Ми, модель: Онюдо"} type={"mythZoan"} rank={"A"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A5%D0%B8%D1%82%D0%BE_%D0%A5%D0%B8%D1%82%D0%BE_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9E%D0%BD%D1%8E%D0%B4%D0%BE"} />
            <DevilFruit name={"Хито Хито но Ми, модель: Ника"} type={"mythZoan"} rank={"S"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%93%D0%BE%D0%BC%D1%83_%D0%93%D0%BE%D0%BC%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8"} />
            <DevilFruit name={"Ину Ину но Ми, модель: Кюби но Кицунэ"} type={"mythZoan"} rank={"A"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%98%D0%BD%D1%83_%D0%98%D0%BD%D1%83_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%9A%D1%8E%D0%B1%D0%B8_%D0%BD%D0%BE_%D0%9A%D0%B8%D1%86%D1%83%D0%BD%D1%8D"} />
            <DevilFruit name={"Хэби Хэби но Ми, модель: Ямата но Ороти"} type={"mythZoan"} rank={"A"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A5%D1%8D%D0%B1%D0%B8_%D0%A5%D1%8D%D0%B1%D0%B8_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%AF%D0%BC%D0%B0%D1%82%D0%B0_%D0%BD%D0%BE_%D0%9E%D1%80%D0%BE%D1%82%D0%B8"} />
            <DevilFruit name={"Уо Уо но Ми, модель: Сэйрю"} used={"used"} type={"mythZoan"} rank={"S"}
                        url={"https://onepiece.fandom.com/ru/wiki/%D0%A3%D0%BE_%D0%A3%D0%BE_%D0%BD%D0%BE_%D0%9C%D0%B8,_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C:_%D0%A1%D1%8D%D0%B9%D1%80%D1%8E"} />
        </details>


    </div>
  );
}

export default App;
