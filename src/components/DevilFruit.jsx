import React from 'react';
import "./DevilFruit.css"

const DevilFruit = ({name, description, cost, used, img, url, type, rank}) => {
    let open = () => {
        window.open(img)
    }

    return (
        <div id={type} className={"card " + type + " " + used}>
            <h1 className={"name"}><a target="_blank" href={url}>{name}</a></h1>
            {img ? <img onClick={open} title={"Click to open full img!"} className={'img'}  src={img} alt="Image here"/> : ''}
            <h3 className={"description"}> {description}</h3>
            <h3 className={"rank " + rank}>{rank} ранг</h3>
        </div>
    );
};

export default DevilFruit;